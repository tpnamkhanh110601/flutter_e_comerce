import 'package:e_commerce/controllers/sign_up_controller.dart';
import 'package:e_commerce/screens/complete_profile/complete_profile_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../components/defaut_button.dart';
import '../../../components/form_error.dart';
import '../../../constants.dart';
import '../../../size_config.dart';

class SignUpForm extends StatefulWidget {
  const SignUpForm({super.key});

  @override
  State<SignUpForm> createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  final SignUpController _signUpController = Get.put(SignUpController());

  @override
  void initState() {
    _signUpController.clearErrorList();
    _signUpController.clearController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        children: [
          customEmailForm(),
          SizedBox(height: getProportionateScreenHeight(30)),
          customPasswordWidget(),
          SizedBox(height: getProportionateScreenHeight(30)),
          customConfirmPassword(),
          SizedBox(height: getProportionateScreenHeight(30)),
          FormError(errors: _signUpController.errors),
          SizedBox(height: getProportionateScreenHeight(40)),
          DefaultButton(
            text: 'Continue',
            press: () async {
              if (_signUpController.validateEmail() &&
                  _signUpController.validatePassword() &&
                  _signUpController.validateConfirmPassword()) {
                bool success_sign_up = await _signUpController.register();
                if (success_sign_up) {
                  _signUpController.errors.remove(kDuplicateEmailError);
                  Navigator.pushNamed(context, CompleteProfileScreen.routeName);
                } else if (success_sign_up == false &&
                    !_signUpController.errors.contains(kDuplicateEmailError)) {
                  _signUpController.errors.add(kDuplicateEmailError);
                }
              }
            },
          ),
        ],
      ),
    );
  }

  Widget customConfirmPassword() {
    return TextFormField(
      controller: _signUpController.confirmPasswordController,
      onChanged: (String value) {
        _signUpController.clearConfirmPasswordErrors();
        return null;
      },
      validator: (String? value) {
        _signUpController.validateConfirmPassword();
        return null;
      },
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'Re-enter your password',
        labelText: 'Confirm Password',
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(
          Icons.lock,
          size: getProportionateScreenHeight(30),
        ),
      ),
    );
  }

  Widget customPasswordWidget() {
    return TextFormField(
      controller: _signUpController.passwordController,
      onChanged: (String value) {
        _signUpController.clearPasswordErrors();
        return null;
      },
      validator: (String? value) {
        _signUpController.validatePassword();
        return null;
      },
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'Enter your password',
        labelText: 'Password',
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(
          Icons.lock,
          size: getProportionateScreenHeight(30),
        ),
      ),
    );
  }

  Widget customEmailForm() {
    return TextFormField(
      controller: _signUpController.emailController,
      keyboardType: TextInputType.emailAddress,
      onChanged: (String value) {
        _signUpController.clearEmailErrors();
        return null;
      },
      validator: (String? value) {
        _signUpController.validateEmail();
        return null;
      },
      decoration: InputDecoration(
        hintText: 'Enter your email',
        labelText: 'Email',
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(
          Icons.email,
          size: getProportionateScreenHeight(30),
        ),
      ),
    );
  }
}
