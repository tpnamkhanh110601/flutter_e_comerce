import 'package:e_commerce/controllers/user_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../components/profile_picture.dart';
import '../../detail_user_profile/detail_user_profile_screen.dart';
import '../../sign_in/sign_in_screen.dart';
import 'profile_menu.dart';

class Body extends StatefulWidget {
  const Body({super.key});

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  final UserController _userController = Get.put(UserController());
  @override
  void initState() {}

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ProfilePicture(),
        const SizedBox(height: 20),
        ProfileMenu(
          iconData: Icons.person,
          text: 'My Account',
          press: () {
            Navigator.pushNamed(context, UserProfileDetailScreen.routeName);
          },
        ),
        ProfileMenu(
          iconData: Icons.notification_important,
          text: 'Notifications',
          press: () {},
        ),
        ProfileMenu(
          iconData: Icons.settings,
          text: 'Settings',
          press: () {},
        ),
        ProfileMenu(
          iconData: Icons.question_mark,
          text: 'Help Center',
          press: () {},
        ),
        ProfileMenu(
          iconData: Icons.logout,
          text: 'Log Out',
          press: () {
            _userController.signOut();
            Navigator.popUntil(
              context,
              ModalRoute.withName(SignInScreen.routeName),
            );
          },
        ),
      ],
    );
  }
}
