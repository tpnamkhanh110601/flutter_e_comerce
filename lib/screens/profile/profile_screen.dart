import 'package:flutter/material.dart';

import '../../enums.dart';
import 'components/body.dart';
import '../../components/custom_bottom_navigator.dart';

class UserProfileScreen extends StatelessWidget {
  static String routeName = '/user_profile';
  const UserProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Profile'),
        centerTitle: true,
      ),
      body: const Body(),
      bottomNavigationBar: const CustomBottomNavigator(
        selectedMenu: MenuState.profile,
      ),
    );
  }
}
