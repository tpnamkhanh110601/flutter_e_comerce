import 'package:e_commerce/controllers/complete_profile_controller.dart';
import 'package:e_commerce/controllers/sign_up_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../components/defaut_button.dart';
import '../../../components/form_error.dart';
import '../../../size_config.dart';
import '../../sign_in/sign_in_screen.dart';

class CompleteProfileForm extends StatefulWidget {
  const CompleteProfileForm({super.key});

  @override
  State<CompleteProfileForm> createState() => _CompleteProfileFormState();
}

class _CompleteProfileFormState extends State<CompleteProfileForm> {
  final CompleteProfileController _completeProfileController =
      Get.put(CompleteProfileController());

  @override
  void initState() {
    _completeProfileController.clearErrorList();
    _completeProfileController.clearController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        children: [
          customFirstNameForm(),
          SizedBox(height: getProportionateScreenHeight(30)),
          customLastNameForm(),
          SizedBox(height: getProportionateScreenHeight(30)),
          customPhoneForm(),
          SizedBox(height: getProportionateScreenHeight(30)),
          customLocationForm(),
          FormError(errors: _completeProfileController.errors),
          SizedBox(height: getProportionateScreenHeight(40)),
          DefaultButton(
            text: 'Continue',
            press: () {
              if (_completeProfileController.validateFirstName() &&
                  _completeProfileController.validatePhone() &&
                  _completeProfileController.validateAddress() &&
                  _completeProfileController.errors.isEmpty) {
                _completeProfileController.storeData();
                Navigator.popUntil(
                  context,
                  ModalRoute.withName(SignInScreen.routeName),
                );
              }
            },
          ),
        ],
      ),
    );
  }

  Widget customFirstNameForm() {
    return TextFormField(
      controller: _completeProfileController.firstNameController,
      keyboardType: TextInputType.emailAddress,
      onChanged: (String value) {
        _completeProfileController.clearFirstNameErrors();
        return null;
      },
      validator: (String? value) {
        _completeProfileController.validateFirstName();
        return null;
      },
      decoration: InputDecoration(
        hintText: 'Enter your first name',
        labelText: 'First Name',
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(
          Icons.person,
          size: getProportionateScreenHeight(30),
        ),
      ),
    );
  }

  Widget customLastNameForm() {
    return TextFormField(
      controller: _completeProfileController.lastNameController,
      decoration: InputDecoration(
        hintText: 'Enter your last name',
        labelText: 'Last Name',
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(
          Icons.person,
          size: getProportionateScreenHeight(30),
        ),
      ),
    );
  }

  Widget customPhoneForm() {
    return TextFormField(
      controller: _completeProfileController.phoneController,
      keyboardType: TextInputType.number,
      onChanged: (String value) {
        _completeProfileController.clearPhoneErrors();
        return null;
      },
      validator: (String? value) {
        _completeProfileController.validatePhone();
        return null;
      },
      decoration: InputDecoration(
        hintText: 'Enter your phone number',
        labelText: 'Phone Number',
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(
          Icons.phone,
          size: getProportionateScreenHeight(30),
        ),
      ),
    );
  }

  Widget customLocationForm() {
    return TextFormField(
      controller: _completeProfileController.addressController,
      onChanged: (String value) {
        _completeProfileController.clearAddressErrors();
        return null;
      },
      validator: (String? value) {
        _completeProfileController.validateAddress();
        return null;
      },
      decoration: InputDecoration(
        hintText: 'Enter your address',
        labelText: 'Address',
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(
          Icons.location_on,
          size: getProportionateScreenHeight(30),
        ),
      ),
    );
  }
}
