import 'package:flutter/material.dart';

import '../../components/rounded_button.dart';
import '../../models/Product_Detail_Argument.dart';
import '../../size_config.dart';
import 'components/body.dart';
import 'components/custom_appbar.dart';

class DetailScreen extends StatelessWidget {
  static String routeName = '/details';
  const DetailScreen({super.key});

  @override
  Widget build(BuildContext context) {
    ProductDetailArguments arg =
        ModalRoute.of(context)?.settings.arguments as ProductDetailArguments;

    return Scaffold(
      backgroundColor: const Color(0xFFF5F6F9),
      appBar: CustomAppBar(
        preferredSize: const Size(10.0, 40.0),
        child: SafeArea(
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(20)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                RoundedBtn(
                  iconData: Icons.arrow_back_ios,
                  press: () => Navigator.pop(context),
                ),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 14, vertical: 5),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(14)),
                  child: Row(
                    children: [
                      Text(
                        arg.product.rating.toString(),
                        style: const TextStyle(
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      const SizedBox(width: 5),
                      const Icon(
                        Icons.star,
                        color: Colors.yellowAccent,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      body: Body(
        product: arg.product,
      ),
    );
  }
}
