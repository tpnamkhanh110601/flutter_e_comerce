import 'package:e_commerce/controllers/sign_in_controller.dart';
import 'package:e_commerce/screens/forgot_password/forgot_password_screen.dart';
import 'package:e_commerce/screens/login_success/login_success_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../components/defaut_button.dart';
import '../../../components/form_error.dart';
import '../../../constants.dart';

import '../../../size_config.dart';

class SignForm extends StatefulWidget {
  const SignForm({super.key});

  @override
  State<SignForm> createState() => _SignFormState();
}

class _SignFormState extends State<SignForm> {
  final SignInController _signInController = Get.put(SignInController());
  bool remember = false;

  @override
  void initState() {
    _signInController.clearErrorList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(children: [
        customEmailForm(),
        SizedBox(height: getProportionateScreenHeight(30)),
        customPasswordWidget(),
        SizedBox(height: getProportionateScreenHeight(30)),
        FormError(errors: _signInController.errors),
        SizedBox(height: getProportionateScreenHeight(30)),
        Row(
          children: [
            Checkbox(
              value: remember,
              activeColor: kPrimaryColor,
              onChanged: (value) {
                setState(() {
                  remember = value ?? false;
                });
              },
            ),
            const Text('Remember me'),
            const Spacer(),
            GestureDetector(
              onTap: () {
                Navigator.pushNamed(context, ForgotPasswordScreen.routeName);
              },
              child: const Text(
                'Forgot Password',
                style: TextStyle(decoration: TextDecoration.underline),
              ),
            ),
          ],
        ),
        SizedBox(height: getProportionateScreenHeight(30)),
        DefaultButton(
          text: 'Continue',
          press: () async {
            if (_signInController.validateEmail() &&
                _signInController.validatePassword()) {
              bool success_sign_in = await _signInController.signIn();
              if (success_sign_in) {
                _signInController.errors.remove(kWrongEmailOrPasswordError);
                Navigator.pushNamed(context, LoginSuccessScreen.routeName);
              } else if (success_sign_in == false &&
                  !_signInController.errors
                      .contains(kWrongEmailOrPasswordError)) {
                _signInController.errors.add(kWrongEmailOrPasswordError);
              }
            }
          },
        ),
      ]),
    );
  }

  Widget customPasswordWidget() {
    return TextFormField(
      controller: _signInController.passwordController,
      onChanged: (String value) {
        _signInController.clearPasswordErrors();
        return null;
      },
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'Enter your password',
        labelText: 'Password',
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(
          Icons.lock,
          size: getProportionateScreenHeight(30),
        ),
      ),
    );
  }

  Widget customEmailForm() {
    return TextFormField(
      controller: _signInController.emailController,
      keyboardType: TextInputType.emailAddress,
      onChanged: (String value) {
        _signInController.clearEmailErrors();
        return null;
      },
      decoration: InputDecoration(
        hintText: 'Enter your email',
        labelText: 'Email',
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(
          Icons.email,
          size: getProportionateScreenHeight(30),
        ),
      ),
    );
  }
}
