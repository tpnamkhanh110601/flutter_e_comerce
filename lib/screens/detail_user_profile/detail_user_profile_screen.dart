import 'package:flutter/material.dart';
import 'components/body.dart';

class UserProfileDetailScreen extends StatelessWidget {
  static String routeName = '/user_profile_detail';
  const UserProfileDetailScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('User Profile Detail'),
        centerTitle: true,
      ),
      body: const Body(),
    );
  }
}
