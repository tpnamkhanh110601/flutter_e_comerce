import 'package:e_commerce/controllers/user_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../components/defaut_button.dart';
import '../../../components/profile_picture.dart';
import '../../../size_config.dart';

class Body extends StatefulWidget {
  const Body({super.key});

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  final UserController _userController = Get.put(UserController());
  @override
  void initState() {
    _userController.readUserData();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: SingleChildScrollView(
          child: Column(
            children: [
              ProfilePicture(),
              SizedBox(height: getProportionateScreenHeight(30)),
              UserInfoTextField(
                labelText: 'First Name',
                controller: _userController.firstNameController,
              ),
              SizedBox(height: getProportionateScreenHeight(20)),
              UserInfoTextField(
                labelText: 'Last Name',
                controller: _userController.lastNameController,
              ),
              SizedBox(height: getProportionateScreenHeight(20)),
              UserInfoTextField(
                labelText: 'Phone Number',
                controller: _userController.phoneController,
              ),
              SizedBox(height: getProportionateScreenHeight(20)),
              UserInfoTextField(
                labelText: 'Address',
                controller: _userController.addressController,
              ),
              SizedBox(height: getProportionateScreenHeight(50)),
              DefaultButton(
                text: 'Edit',
                press: () {
                  // Navigator.pushNamed(
                  //         context, UserProfileDetailEditScreen.routeName)
                  //     .then((value) {
                  // userProfileService.readData();
                  // setState(() {
                  //   networkImagePath = url;
                  // });
                  // });
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget UserInfoTextField({labelText, controller}) {
    return TextFormField(
      controller: controller,
      decoration: InputDecoration(
        labelText: labelText,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
          borderSide: const BorderSide(
            width: 1,
            color: Colors.black,
            style: BorderStyle.solid,
          ),
        ),
      ),
      enabled: false,
    );
  }
}
