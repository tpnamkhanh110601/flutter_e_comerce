import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../size_config.dart';
import 'category_card.dart';

class Category extends StatelessWidget {
  List<Map<dynamic, dynamic>> categories = [
    {
      'icon': Icons.bolt,
      'text': 'Flash Deal',
    },
    {
      'icon': FontAwesomeIcons.fileInvoiceDollar,
      'text': 'Bill',
    },
    {
      'icon': Icons.gamepad,
      'text': 'Game',
    },
    {
      'icon': FontAwesomeIcons.gift,
      'text': 'Daily Gift',
    },
    {
      'icon': Icons.more,
      'text': 'More',
    },
  ];
  Category({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: List.generate(
          categories.length,
          (index) => CategoryCard(
            icon: categories[index]['icon'],
            text: categories[index]['text'],
            press: () {},
          ),
        ),
      ),
    );
  }
}
