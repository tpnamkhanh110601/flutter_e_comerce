import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../size_config.dart';
import 'icon_button_with_counter.dart';
import 'search_field.dart';

class HomeHeader extends StatelessWidget {
  const HomeHeader({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const SearchField(),
          // IconBtnWithCounter(
          //   icon: FontAwesomeIcons.cartShopping,
          //   press: () => Navigator.pushNamed(context, CartScreen.routeName),
          // ),
          IconBtnWithCounter(
            icon: FontAwesomeIcons.bell,
            numOfItems: 3,
            press: () {},
          ),
        ],
      ),
    );
  }
}
