import 'package:flutter/material.dart';

import '../../../size_config.dart';
import 'category.dart';
import 'discount_banner.dart';
import 'home_header.dart';
import 'popular_product.dart';
import 'product_card.dart';
import 'special_offer.dart';

class Body extends StatelessWidget {
  const Body({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: getProportionateScreenWidth(20)),
            const HomeHeader(),
            SizedBox(height: getProportionateScreenWidth(30)),
            const DiscountBanner(),
            SizedBox(height: getProportionateScreenWidth(20)),
            Category(),
            SizedBox(height: getProportionateScreenWidth(20)),
            const SpecialOffer(),
            SizedBox(height: getProportionateScreenWidth(20)),
            const PopularProduct(),
            SizedBox(height: getProportionateScreenWidth(20)),
          ],
        ),
      ),
    );
  }
}
