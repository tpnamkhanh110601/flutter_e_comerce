import 'package:flutter/material.dart';

import '../../../models/Product.dart';
import '../../../size_config.dart';
import 'product_card.dart';
import 'section_title.dart';

class PopularProduct extends StatelessWidget {
  const PopularProduct({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SectionTitle(text: 'Popular Product', press: () {}),
        SizedBox(height: getProportionateScreenWidth(20)),
        // SingleChildScrollView(
        //   scrollDirection: Axis.horizontal,
        //   child: Row(
        //     children: [
        //       ...List.generate(
        //         demoProducts.length,
        //         (index) => ProductCard(
        //           product: demoProducts[index],
        //           press: () => Navigator.pushNamed(
        //             context,
        //             DetailScreen.routeName,
        //             arguments: ProductDetailArguments(
        //               product: demoProducts[index],
        //             ),
        //           ),
        //         ),
        //       ),
        //       SizedBox(width: getProportionateScreenWidth(20)),
        //     ],
        //   ),
        // ),
      ],
    );
  }
}
