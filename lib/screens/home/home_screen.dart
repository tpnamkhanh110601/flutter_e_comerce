import 'package:flutter/material.dart';

import '../../components/custom_bottom_navigator.dart';
import '../../enums.dart';
import 'components/body.dart';

class HomeScreen extends StatelessWidget {
  static String routeName = '/home';
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Body(),
      bottomNavigationBar: CustomBottomNavigator(selectedMenu: MenuState.home),
    );
  }
}
