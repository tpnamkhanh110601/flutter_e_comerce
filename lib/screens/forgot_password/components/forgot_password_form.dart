import 'package:e_commerce/constants.dart';
import 'package:e_commerce/controllers/reset_password_controller.dart';
import 'package:e_commerce/screens/sign_in/sign_in_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:quickalert/quickalert.dart';

import '../../../../size_config.dart';
import '../../../components/defaut_button.dart';
import '../../../components/form_error.dart';
import '../../../components/no_account.dart';

class ForgotPasswordForm extends StatefulWidget {
  const ForgotPasswordForm({super.key});

  @override
  State<ForgotPasswordForm> createState() => _ForgotPasswordFormState();
}

class _ForgotPasswordFormState extends State<ForgotPasswordForm> {
  final ForgotPasswordController _forgotPasswordController =
      Get.put(ForgotPasswordController());

  @override
  void initState() {
    _forgotPasswordController.clearController();
    _forgotPasswordController.clearErrorList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        children: [
          SizedBox(height: SizeConfig.screenHeight * 0.04),
          customEmailForm(),
          SizedBox(height: getProportionateScreenHeight(30)),
          FormError(errors: _forgotPasswordController.errors),
          SizedBox(height: SizeConfig.screenHeight * 0.1),
          DefaultButton(
              text: 'Continue',
              press: () async {
                if (_forgotPasswordController.validateEmail()) {
                  bool resetPasswordSuccess =
                      await _forgotPasswordController.resetPassword();
                  if (resetPasswordSuccess) {
                    _forgotPasswordController.errors
                        .remove(kNotExistEmailError);
                    QuickAlert.show(
                      context: context,
                      type: QuickAlertType.success,
                      title: 'Successfully!',
                      text: 'Reset password link has been sent to your email',
                      onConfirmBtnTap: () {
                        return Navigator.popUntil(
                          context,
                          ModalRoute.withName(SignInScreen.routeName),
                        );
                      },
                    );
                  } else if (resetPasswordSuccess == false &&
                      !_forgotPasswordController.errors
                          .contains(kNotExistEmailError)) {
                    _forgotPasswordController.errors.add(kNotExistEmailError);
                  }
                }
              }),
          SizedBox(height: SizeConfig.screenHeight * 0.1),
          const NoAccount(),
        ],
      ),
    );
  }

  Widget customEmailForm() {
    return TextFormField(
      controller: _forgotPasswordController.emailController,
      keyboardType: TextInputType.emailAddress,
      onChanged: (String value) {
        _forgotPasswordController.clearEmailErrors();
      },
      decoration: InputDecoration(
        hintText: 'Enter your email',
        labelText: 'Email',
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(
          Icons.email,
          size: getProportionateScreenHeight(30),
        ),
      ),
    );
  }
}
