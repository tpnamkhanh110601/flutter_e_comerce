import 'package:e_commerce/controllers/authentication_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../constants.dart';

class CompleteProfileController extends GetxController {
  final AuthenticationController _authenticationController =
      Get.put(AuthenticationController());
  RxList<String> errors = [''].obs;
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController addressController = TextEditingController();

  void clearErrorList() {
    errors.clear();
  }

  void clearController() {
    firstNameController.clear();
    lastNameController.clear();
    phoneController.clear();
    addressController.clear();
  }

  bool validateFirstName() {
    if (firstNameController.text.isEmpty && !errors.contains(kNamelNullError)) {
      errors.add(kNamelNullError);
      return false;
    }
    return true;
  }

  void clearFirstNameErrors() {
    if (firstNameController.text.isNotEmpty &&
        errors.contains(kNamelNullError)) {
      errors.remove(kNamelNullError);
    }
  }

  bool validatePhone() {
    if (phoneController.text.isEmpty &&
        !errors.contains(kPhoneNumberNullError)) {
      errors.add(kPhoneNumberNullError);
      return false;
    }
    return true;
  }

  void clearPhoneErrors() {
    if (phoneController.text.isNotEmpty &&
        errors.contains(kPhoneNumberNullError)) {
      errors.remove(kPhoneNumberNullError);
    }
  }

  bool validateAddress() {
    if (addressController.text.isEmpty && !errors.contains(kAddressNullError)) {
      errors.add(kAddressNullError);
      return false;
    }
    return true;
  }

  void clearAddressErrors() {
    if (addressController.text.isNotEmpty &&
        errors.contains(kAddressNullError)) {
      errors.remove(kAddressNullError);
    }
  }

  void storeData() async {
    Map<String, dynamic> userData = {
      'imgUrl': '',
      'firstName': firstNameController.text.trim(),
      'lastName': lastNameController.text.trim(),
      'phone': phoneController.text.trim(),
      'address': addressController.text.trim(),
    };
    await _authenticationController.writeData(table: 'users', data: userData);
  }
}
