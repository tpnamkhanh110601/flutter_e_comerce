import 'package:get/get.dart';

class ProfilePictureController extends GetxController {
  String defaultImg = 'assets/images/user.jpeg';
  RxString imageUrl = 'assets/images/user.jpeg'.obs;
  bool localImage = false;
  bool networkImage = false;
}
