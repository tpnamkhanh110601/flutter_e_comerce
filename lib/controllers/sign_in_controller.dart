import 'package:e_commerce/controllers/authentication_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../constants.dart';

class SignInController extends GetxController {
  final AuthenticationController _authenticationController =
      Get.put(AuthenticationController());
  RxList<String> errors = [''].obs;
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  void clearErrorList() {
    errors.clear();
  }

  void clearController() {
    emailController.clear();
    passwordController.clear();
  }

  bool validateEmail() {
    if (errors.contains('')) {
      errors.remove('');
    }
    if (emailController.text.isEmpty && !errors.contains(kEmailNullError)) {
      errors.add(kEmailNullError);
      return false;
    } else if (!emailValidatorRegExp.hasMatch(emailController.text) &&
        !errors.contains(kInvalidEmailError) &&
        emailController.text.isNotEmpty) {
      errors.add(kInvalidEmailError);
      return false;
    }
    return true;
  }

  void clearEmailErrors() {
    if (emailController.text.isNotEmpty && errors.contains(kEmailNullError)) {
      errors.remove(kEmailNullError);
    } else if (emailValidatorRegExp.hasMatch(emailController.text) &&
        errors.contains(kInvalidEmailError)) {
      errors.remove(kInvalidEmailError);
    }
  }

  bool validatePassword() {
    if (passwordController.text.isEmpty && !errors.contains(kPassNullError)) {
      errors.add(kPassNullError);
      return false;
    } else if (passwordController.text.length < 8 &&
        !errors.contains(kShortPassError) &&
        passwordController.text.isNotEmpty) {
      errors.add(kShortPassError);
      return false;
    }
    return true;
  }

  void clearPasswordErrors() {
    errors.remove(kWrongEmailOrPasswordError);
    if (passwordController.text.isNotEmpty && errors.contains(kPassNullError)) {
      errors.remove(kPassNullError);
    } else if (passwordController.text.length >= 8 &&
        errors.contains(kShortPassError)) {
      errors.remove(kShortPassError);
    }
  }

  Future signIn() async {
    try {
      bool success_sign_in =
          await _authenticationController.signInWithEmailAndPassword(
              emailController.text.trim(), passwordController.text.trim());
      if (success_sign_in) {
        return true;
      }
      return false;
    } catch (e) {
      print(e);
      return false;
    }
  }
}
