import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';

class AuthenticationController extends GetxController {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final database = FirebaseFirestore.instance;

  Future signInWithEmailAndPassword(String email, String password) async {
    try {
      await _auth.signInWithEmailAndPassword(email: email, password: password);
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future signOut() async {
    await _auth.signOut();
  }

  Future signUpWithEmailAndPassword(String email, String password) async {
    try {
      await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future resetPassword(String email) async {
    try {
      await _auth.sendPasswordResetEmail(email: email);
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future writeData({table, data}) async {
    final docUser = database.collection(table).doc(getUserId());
    await docUser.set(data);
  }

  Future readData({table}) async {
    final data = database.collection(table).doc(getUserId()).get();
    return data;
  }

  Future updateData({table, data}) async {
    final docUser = database.collection(table).doc(getUserId());
    await docUser.update(data);
  }

  String getUserId() {
    final User user = _auth.currentUser as User;
    final uid = user.uid;
    return uid.toString();
  }
}
