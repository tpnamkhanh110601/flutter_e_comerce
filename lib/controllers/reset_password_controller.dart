import 'package:e_commerce/controllers/authentication_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../constants.dart';

class ForgotPasswordController extends GetxController {
  final AuthenticationController _authenticationController =
      Get.put(AuthenticationController());
  RxList<String> errors = [''].obs;
  TextEditingController emailController = TextEditingController();

  void clearErrorList() {
    errors.clear();
  }

  void clearController() {
    emailController.clear();
  }

  bool validateEmail() {
    clearErrorList();
    if (emailController.text.isEmpty && !errors.contains(kEmailNullError)) {
      errors.add(kEmailNullError);
      return false;
    } else if (!emailValidatorRegExp.hasMatch(emailController.text) &&
        !errors.contains(kInvalidEmailError) &&
        emailController.text.isNotEmpty) {
      errors.add(kInvalidEmailError);
      return false;
    } else if (errors.length > 0) {
      return false;
    }
    return true;
  }

  void clearEmailErrors() {
    if (emailController.text.isNotEmpty && errors.contains(kEmailNullError)) {
      errors.remove(kEmailNullError);
    } else if (emailValidatorRegExp.hasMatch(emailController.text) &&
        errors.contains(kInvalidEmailError)) {
      errors.remove(kInvalidEmailError);
    }
  }

  Future resetPassword() async {
    bool resetPasswordSuccess = await _authenticationController
        .resetPassword(emailController.text.trim());
    if (resetPasswordSuccess) {
      return true;
    }
    return false;
  }
}
