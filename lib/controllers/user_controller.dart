import 'package:e_commerce/components/profile_picture.dart';
import 'package:e_commerce/controllers/authentication_controller.dart';
import 'package:e_commerce/controllers/profile_picture_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class UserController extends GetxController {
  final AuthenticationController _authenticationController =
      Get.put(AuthenticationController());
  final ProfilePictureController _profilePictureController =
      Get.put(ProfilePictureController());
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController addressController = TextEditingController();

  void signOut() {
    _authenticationController.signOut();
  }

  void readUserData() async {
    var data = await _authenticationController.readData(table: 'users');
    if (data['imgUrl'] != '') {
      _profilePictureController.imageUrl.value = data['imgUrl'];
      _profilePictureController.networkImage = true;
    } else {
      _profilePictureController.networkImage = false;
    }
    firstNameController.text = data['firstName'];
    lastNameController.text = data['lastName'];
    phoneController.text = data['phone'];
    addressController.text = data['address'];
  }
}
