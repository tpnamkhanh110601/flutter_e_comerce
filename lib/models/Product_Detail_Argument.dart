import 'Product.dart';

class ProductDetailArguments {
  final Product product;

  ProductDetailArguments({required this.product});
}
