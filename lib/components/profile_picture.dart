import 'dart:io';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:quickalert/quickalert.dart';

import '../controllers/profile_picture_controller.dart';

class ProfilePicture extends StatefulWidget {
  bool editMode;
  ProfilePicture({
    super.key,
    this.editMode = false,
  });

  @override
  State<ProfilePicture> createState() => _ProfilePictureState();
}

class _ProfilePictureState extends State<ProfilePicture> {
  final ProfilePictureController _profilePictureController =
      Get.put(ProfilePictureController());

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 115,
      width: 115,
      child: Stack(
        fit: StackFit.expand,
        clipBehavior: Clip.none,
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(150),
              border: Border.all(
                width: 1,
                style: BorderStyle.solid,
                color: Colors.grey,
              ),
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(150),
              child: readImage(),
            ),
          ),
          widget.editMode
              ? Positioned(
                  bottom: 0,
                  right: -12,
                  child: SizedBox(
                    height: 46,
                    width: 46,
                    child: ElevatedButton(
                      onPressed: () {
                        QuickAlert.show(
                          context: context,
                          type: QuickAlertType.custom,
                          text: 'Select upload image type',
                          widget: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              selectImageType(
                                lable: 'Take an image',
                                press: () {
                                  // Navigator.pushNamed(
                                  //         context, CameraScreen.routeName)
                                  //     .then(
                                  //   (value) {
                                  //     setState(
                                  //       () {
                                  //         getImgPath();
                                  //       },
                                  //     );
                                  //     return;
                                  //   },
                                  // );
                                },
                              ),
                              selectImageType(lable: 'Upload from gallery'),
                            ],
                          ),
                          confirmBtnText: 'Cancel',
                          confirmBtnColor: Colors.grey,
                        );
                      },
                      style: ElevatedButton.styleFrom(
                        backgroundColor: const Color(0xFFF5F6F9),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50),
                          side: const BorderSide(
                            color: Colors.white,
                          ),
                        ),
                      ),
                      child: const Icon(
                        FontAwesomeIcons.camera,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                )
              : const SizedBox(),
        ],
      ),
    );
  }

  Widget readImage() {
    if (_profilePictureController.localImage) {
      return Obx(
        () => Image.file(
          File(_profilePictureController.imageUrl.value),
          fit: BoxFit.fill,
        ),
      );
    }
    if (_profilePictureController.networkImage) {
      return Obx(() => Image.network(
            _profilePictureController.imageUrl.value,
            fit: BoxFit.fill,
          ));
    }
    return Image.asset(
      _profilePictureController.defaultImg,
      colorBlendMode: BlendMode.color,
    );
  }

  Widget selectImageType({lable, press}) {
    return Container(
      alignment: Alignment.centerLeft,
      height: 50,
      width: double.infinity,
      margin: const EdgeInsets.symmetric(vertical: 10),
      padding: const EdgeInsets.only(left: 10),
      decoration: BoxDecoration(
        border: Border.all(
          width: 1,
          color: Colors.black,
          style: BorderStyle.solid,
        ),
        borderRadius: BorderRadius.circular(20),
      ),
      child: GestureDetector(
        onTap: press,
        child: Text(lable),
      ),
    );
  }
}
