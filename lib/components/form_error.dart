import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../size_config.dart';

class FormError extends StatefulWidget {
  RxList<String> errors;
  FormError({super.key, required this.errors});

  @override
  State<FormError> createState() => _FormErrorState();
}

class _FormErrorState extends State<FormError> {
  @override
  Widget build(BuildContext context) {
    return Obx(() => Column(
          children: List.generate(widget.errors.length,
              (index) => formErrorText(error: widget.errors.value[index])),
        ));
  }

  Row formErrorText({error}) {
    return Row(
      children: [
        const Icon(
          Icons.error,
          color: Colors.red,
        ),
        SizedBox(width: getProportionateScreenWidth(10)),
        Text(error),
      ],
    );
  }
}
